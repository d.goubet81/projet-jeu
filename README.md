# projet-jeu

Le jeu réalisé est un puissance 4.

## Déroulé du jeu

- Le joueur 1 est invités à saisir son nom puis à choisir la couleur de son jeton.
- Le joueur 2 est invités à faire la même chose.
- La grille s'affiche dans un premier temps puis entre chaque tour de jeu jusqu'à ce qu'il y ait un vaiqueur.
- Les joueurs choisissent à tour de role la colonne dans laquelle ils souhaitent placer leur jeton.
- Le jeu prend fin lorsqu'il y a un vainqueur et le nom de celui-ci est affiché.
- La partie s'arrêtera également d'elle-même lorsque la grille sera pleine
- Les joueurs ont également la possibilité de mettre fin à la partie durant leur tour de jeu en saisissant 99

## Classes

- **App** : lance la partie en donnant le nombre de lignes et de colonnes de la grille ainsi que le nombre de jetons à aligner pour gagner.
- **game.Game** : gère le déroulé du jeu.
- **game.Grid** : grille du jeu.
- **game.Player** : contient les informations (nom, couleur jeton) des joueurs.
- **game.Colors** : différentes couleurs pouvant être jouées.
- **interfaces.input** : gère les saisies clavier (Scanner).
- **interfaces.output** : gère les affichages (System.out.print).
- **utilitaries.Checker** : contient les méthodes de contrôle.

## App

- Crée l'instance **game**
- Evolution : possible de demander aux joueurs la taille de la grille et le nombre de jetons à aligner.

## game.Game

- Crée les instances **grid**, **checker**, **player**, **colors**, **input** et **output**
- La méthode **play** exécute le déroulé du jeu:
  * Création des joueurs.
  * Boucle tant qu'il n'y a pas de vainqueur ou que la grille n'est pas pleine :
    * Affichage de la grille
    * Demande de la colonne au joueur actuel
    * Teste si le joueur actuel a aligné suffisamment de jetons
    * Si pas suffisamment de jetons alignés, changement de joueur
  * Affichage du vainqueur.
- La méthode **playerChange** change la valeur du joueur actuel.

## game.Grid

- *tokens*, tableau d'entier à deux dimenssions qui mémorise la position des jetons.
- le *token[0][0]* est dans le coin haut-gauche.
- La méthode **setTokens** permet de positionner le jeton dans *tokens* en fonction de la colonne choisi par le joueur.

## game.Player

- Mémorise le nom et la couleur du jeton.


## game.Colors

- Défini les couleurs pouvant être choisi pour les jetons.
- **selectColor** permet de vérouiller la couleur pour qu'elle ne puisse pas être choisie de nouveau.
- **showAvailable** affiche les choix possible aux joueurs.
- **setTextInColor** formate un string pour qu'il soit afficher en couleur.

## interfaces.input

- Crée l'instance **Scanner**
- Contient les méthodes permettant de demander au joueur son nom et sa couleur de jeton au début du jeu.
- Contient la méthode demandant au joueur son choix de colonne.

## interfaces.output

- Contient les méthodes pour afficher la grille, le tour du joueur, le vainqueur et faire les affichages dans la console.

## utilitaries.Checker

- **checkNumeric** contrôle que la chaine de caractère contient un nombre.
- **checkIntegerRange** contrôle que l'entier est bien dans la plage voulue.
- **ctrlCurPlayerWin** scanne la grille à la recherche d'un alignement de jeton.
- **findAlignment** permet de rechercher un alignement dans une direction donnée.
- **tokensAlignPossible** cherche si l'alignement est possible dans la direction demandée.
- **checkColFull** et **checkGridFull** contrôlent respectivement si une colonne ou si la grille est pleine.  
