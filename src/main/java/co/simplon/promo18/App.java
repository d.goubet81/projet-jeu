package co.simplon.promo18;

import co.simplon.promo18.game.Game;

/**
 * 
 * Connect Four
 * 
 */
public class App {
  public static void main(String[] args) {
    int gameLineSize = 6;
    int gameColSize = 7;
    int tokensToAlign = 4;

    Game game = new Game(gameLineSize, gameColSize, tokensToAlign);

    game.play();

  }
}
