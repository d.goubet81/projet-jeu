package co.simplon.promo18.interfaces;

import co.simplon.promo18.game.Colors;
import co.simplon.promo18.game.Player;

public class Output {

  private Colors colors;

  public Output(Colors colors) {
    this.colors = colors;
  }

  public void clearCli() {
    print("\033[H\033[2J");
    System.out.flush();
  }

  public void displayRules(int tokensToAlign) {
    clearCli();
    print("************ Puissance 4 ************\n");
    print("Le but du jeu est d'aligner " + tokensToAlign + " jetons \n");
    print("Ceux-ci peuvent être alignés de façon\n");
    print("horizontale, verticale ou diagonale  \n\n");
  }

  public void displayGrid(int[][] tokens) {
    for (int col = 0; col < tokens[0].length; col++) {
      print("|");
      print(String.valueOf(col + 1));
    }
    print("|\n");
    for (int[] gridLine : tokens) {
      for (int token : gridLine) {
        print("|");;
        if (token != 0) { // Jeton joueur
          print(colors.setTextInColor("\u25CF", token));
        } else { // Pas de jeton
          print(" ");
        }
      }
      print("|\n");
    }
    print("\n");
  }

  public void showCurrentPlayer(String name) {
    print("Au tour de " + name + "\n");
  }

  public void showWinner(int winner, Player player1, Player player2) {
    switch (winner) {
      case 1:
        print(colors.setTextInColor("Victoire de " + player1.getName() + "\n", player1.getColor()));
        break;
      case 2:
        print(colors.setTextInColor("Victoire de " + player2.getName() + "\n", player2.getColor()));
        break;
      default:
        print("Pas de vainqueur\n");
        break;
    }
  }

  public static void print(String text) {
    System.out.print("\u001B[37m" + text + "\u001B[0m");
  }

}
