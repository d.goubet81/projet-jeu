package co.simplon.promo18.interfaces;

import java.util.Scanner;
import co.simplon.promo18.game.Colors;
import co.simplon.promo18.game.Player;
import co.simplon.promo18.utilitaries.Checker;

public class Input {
  private int quitValue;

  private Checker checker;
  private Colors colors;
  private Scanner sc = new Scanner(System.in);

  public Input(Checker checker, Colors colors, int quitValue) {
    this.checker = checker;
    this.colors = colors;
    this.quitValue = quitValue;
  }

  public Player setPlayer(int numPlayer) {
    String name = askPlayerName(numPlayer);
    Output.print("\n");
    int color = askPlayerColor();
    Output.print("\n");

    Player player = new Player(name, color);
    return player;
  }

  private String askPlayerName(int numPlayer) {
    String name = "";

    while (name.isEmpty()) {
      Output.print("Saisir le nom du Joueur " + (numPlayer + 1) + " :\n");
      name = getString();
    }
    return name;
  }

  private int askPlayerColor() {
    int color = 0;
    boolean choiceOK = false;

    Output.print("Choisir un couleur :\n");
    colors.showAvailable();
    while (!choiceOK) {
      color = getInteger();
      if (checker.checkIntegerRange(color, 1, colors.getLength())) {
        choiceOK = colors.selectColor(color - 1);
      }
      if (!choiceOK) {
        Output.print("Choix non valide\n");
      }
    }
    return color;
  }

  public int choiceColumn(int[][] tokens) {
    int choice;
    boolean choiceOK = false;

    do {
      Output.print("Choisir une colonne entre 1 et " + tokens[0].length + " (" + quitValue
          + " pour quitter) :\n");
      choice = getInteger();
      if (choice != quitValue) {
        if (checker.checkIntegerRange(choice, 1, tokens[0].length)) {
          if (checker.checkColFull(tokens, choice - 1)) {
            Output.print("Cette colonne est pleine.\n");
          } else {
            choiceOK = true;
          }
        } else {
          Output.print("La valeur doit être comprise entre 1 et " + tokens[0].length + "\n");
        }
      } else {
        choiceOK = true;
      }
    } while (!choiceOK);
    return choice;
  }

  public int getInteger() {
    String input;
    do {
      input = sc.nextLine();
    } while (!checker.checkNumeric(input));

    return Integer.valueOf(input);
  }

  public String getString() {
    return sc.nextLine();
  }


  public void closeScanner() {
    sc.close();
  }

}
