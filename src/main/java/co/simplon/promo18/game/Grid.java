package co.simplon.promo18.game;

public class Grid {
  private int gameLineSize;
  private int gameColSize;

  private int[][] tokens;

  public Grid(int gameLineSize, int gameColSize) {
    this.gameColSize = gameColSize;
    this.gameLineSize = gameLineSize;
    tokens = new int[this.gameLineSize][this.gameColSize];
  }

  public int[][] getTokens() {
    return tokens;
  }

  public void setTokens(int position, int colorNumber) {
    int line = 0;

    while (line < tokens.length && tokens[line][position] == 0) {
      tokens[line][position] = colorNumber;
      if (line > 0) {
        tokens[line - 1][position] = 0;
      }
      line++;
    }
  }
}
