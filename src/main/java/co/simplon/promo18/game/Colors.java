package co.simplon.promo18.game;

import co.simplon.promo18.interfaces.Output;

public class Colors {
  private String[] label = {"Red", "Green", "Yellow", "Blue", "Purple"};
  private String[] ansi = {"\u001B[31m", "\u001B[32m", "\u001B[33m", "\u001B[34m", "\u001B[35m"};
  private String ansiReset = "\u001B[0m";
  private boolean[] selected = new boolean[label.length];
  private int length = label.length;

  public String[] getLabel() {
    return label;
  }

  public boolean getSelected(int number) {
    return selected[number];
  }

  public boolean selectColor(int number) {
    boolean choiceOK = false;

    if (!selected[number]) {
      selected[number] = true;
      choiceOK = true;
    }
    return choiceOK;
  }

  public String setTextInColor(String text, int color) {
    return ansi[color - 1] + text + ansiReset;
  }

  public void showAvailable() {
    for (int i = 0; i < label.length; i++) {
      if (!selected[i]) {
        Output.print((i + 1) + " = " + ansi[i] + label[i] + ansiReset + "\n");
      }
    }
  }

  public int getLength() {
    return length;
  }
}
