package co.simplon.promo18.game;

import co.simplon.promo18.interfaces.Input;
import co.simplon.promo18.interfaces.Output;
import co.simplon.promo18.utilitaries.Checker;

public class Game {
  private int gameLineSize;
  private int gameColSize;
  private int tokensToAlign;

  private Player[] player = new Player[2];

  private int currentPlayer = 1;
  private int quitValue = 99;

  private Grid grid;
  private Checker checker;
  private Colors colors;
  private Input input;
  private Output output;


  public Game(int gameLineSize, int gameColSize, int tokensToAlign) {
    this.gameLineSize = gameLineSize;
    this.gameColSize = gameColSize;
    this.tokensToAlign = tokensToAlign;

    this.grid = new Grid(this.gameLineSize, this.gameColSize);
    this.checker = new Checker(this.tokensToAlign);
    this.colors = new Colors();
    this.input = new Input(this.checker, this.colors, this.quitValue);
    this.output = new Output(this.colors);
  }

  public void play() {
    boolean endOfGame = false;
    int idWinner = 0;

    output.displayRules(tokensToAlign);

    for (int i = 0; i < player.length; i++) {
      player[i] = input.setPlayer(i);
    }

    output.displayGrid(grid.getTokens());
    output.showCurrentPlayer(player[currentPlayer - 1].getName());
    do {
      int choice = input.choiceColumn(grid.getTokens());
      if (choice != quitValue) {
        grid.setTokens(choice - 1, player[currentPlayer - 1].getColor());
        output.displayGrid(grid.getTokens());

        if (checker.ctrlCurPlayerWin(grid.getTokens(), player[currentPlayer - 1].getColor())) {
          idWinner = currentPlayer;
        }

        endOfGame = idWinner > 0 || checker.checkGridFull(grid.getTokens());
        if (!endOfGame) {
          playerChange();
          output.showCurrentPlayer(player[currentPlayer - 1].getName());
        }
      } else {
        break;
      }
    } while (!endOfGame);

    output.showWinner(idWinner, player[0], player[1]);


    input.closeScanner();
  }

  public void playerChange() {
    if (currentPlayer == 1) {
      currentPlayer = 2;
    } else {
      currentPlayer = 1;
    }
  }
}
