package co.simplon.promo18.utilitaries;

import co.simplon.promo18.interfaces.Output;

public class Checker {

  private int tokensToAlign;

  public Checker(int tokensToAlign) {
    this.tokensToAlign = tokensToAlign;
  }

  public boolean checkNumeric(String input) {
    boolean isNumeric = false;
    {
      if (!input.isEmpty())
        for (int i = 0; i < input.length(); ++i) {
          isNumeric = input.charAt(i) >= 48 && input.charAt(i) <= 57;
          if (!isNumeric) {
            Output.print("Veuillez saisir un nombre.\n");
            break;
          }
        }
    }
    return isNumeric;
  }
  
  public boolean checkIntegerRange(int value, int min, int max) {
    boolean result = false;

    if (value >= min && value <= max) {
      result = true;
    }
    return result;
  }


  public boolean ctrlCurPlayerWin(int[][] tokens, int colorNumber) {
    boolean hasWinner = false;

    int lineIndex = 0;
    while (lineIndex < tokens.length && !hasWinner) {
      int colIndex = 0;
      while (colIndex < tokens[lineIndex].length && !hasWinner) {
        if (tokens[lineIndex][colIndex] == colorNumber) {
          // Test alignement horizontal
          hasWinner = findAlignment(tokens, lineIndex, colIndex, colorNumber, 0, 1);
          if (!hasWinner) { // Test alignement vertical
            hasWinner = findAlignment(tokens, lineIndex, colIndex, colorNumber, 1, 0);
            if (!hasWinner) { // Test alignement diagonal à droite
              hasWinner = findAlignment(tokens, lineIndex, colIndex, colorNumber, 1, 1);
              if (!hasWinner) { // Test alignement diagonal à gauche
                hasWinner = findAlignment(tokens, lineIndex, colIndex, colorNumber, 1, -1);
              }
            }
          }
        }
        colIndex++;
      }
      lineIndex++;
    }
    return hasWinner;
  }

  private boolean findAlignment(int[][] tokens, int lineNumber, int colNumber, int currentPlayer,
      int lineShift, int colShift) {

    boolean hasWinner = false;
    int idtTokens = 1;

    if (tokensAlignPossible(lineNumber, colNumber, tokens.length, tokens[0].length, lineShift,
        colShift)) {
      while (idtTokens < tokensToAlign && tokens[lineNumber + idtTokens * lineShift][colNumber
          + idtTokens * colShift] == currentPlayer) {
        idtTokens++;
      }
      if (idtTokens >= tokensToAlign) {
        hasWinner = true;
      }
    }
    return hasWinner;
  }

  private boolean tokensAlignPossible(int line, int col, int lineSize, int colSize, int lineShift,
      int colShift) {
    boolean result = false;

    boolean horizontalCheck = col * colShift <= (colSize - tokensToAlign) * colShift;
    boolean verticalCheck = line * lineShift <= (lineSize - tokensToAlign) * lineShift;

    if (verticalCheck && horizontalCheck) {
      result = true;
    }
    return result;
  }

  public boolean checkColFull(int[][] tokens, int col) {
    return tokens[0][col] != 0;
  }

  public boolean checkGridFull(int[][] tokens) {
    boolean isFull = true;

    for (int col = 0; col < tokens[0].length; col++) {
      if (tokens[0][col] == 0) {
        isFull = false;
        break;
      }
    }
    return isFull;
  }

}
